#!/usr/bin/env python2.7

from flup.server.fcgi import WSGIServer

import sys, os
sys.path.append(os.path.dirname(os.path.realpath(__file__)))

from extra import app as application

if __name__ == '__main__':
  WSGIServer(application).run()
