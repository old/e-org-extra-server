from flask import Flask, Response
from flask import abort, render_template, send_from_directory
from flask_restful import Resource, Api, abort as api_abort

from extra.data import THEMES, BACKGROUNDS

import csv
import io

app = Flask(__name__)
api = Api(app)

@app.route('/')
def welcome():
    return render_template('welcome.html', themes=THEMES, backgrounds=BACKGROUNDS)

@app.route('/about/')
def about():
    return render_template('about.html')

class ThemeList(Resource):
    def get(self):
        return THEMES

class Theme(Resource):
    def get(self, theme_id):
        if theme_id not in THEMES:
            api_abort(404, message="Theme {} not found".format(theme_id))

        return THEMES[theme_id]

@app.route('/themes/<path:filename>.edj')
def themes_assets(filename):
    return send_from_directory(app.root_path + '/assets/themes/', filename + '.edj')

@app.route('/themes/preview/<path:imagename>.png')
def themes_preview_assets(imagename):
    return send_from_directory(app.root_path + '/assets/themes/preview/', imagename + '.png')

@app.route('/themes/')
@app.route('/themes/<theme_id>')
def themes(theme_id=None):
    if theme_id:
        if theme_id not in THEMES:
            abort(404)
        return render_template('theme.html', theme_id=theme_id, theme=THEMES[theme_id])
    else:
        return render_template('themes.html', themes=THEMES)

@api.representation('text/csv')
def csvformat(data, code, headers=None):
    dw = io.StringIO()
    out = csv.writer(dw, quoting=csv.QUOTE_NONNUMERIC)
    for key, value in data.items():
        if isinstance(value, dict):
            out.writerow([key])
        else:
            out.writerow([key, value])
    resp = Response(dw.getvalue(), 'text/csv', headers=headers)
    resp.status_code = code
    return resp

api.add_resource(ThemeList, '/v1/themes/')
api.add_resource(Theme, '/v1/themes/<string:theme_id>')

class BackgroundList(Resource):
    def get(self):
        return BACKGROUNDS

class Background(Resource):
    def get(self, bg_id):
        if bg_id not in BACKKGROUNDS:
            api_abort(404, message="Background {} not found".format(theme_id))

        return BACKGROUNDS[theme_id]

@app.route('/backgrounds/<path:filename>.edj')
def backgrounds_assets(filename):
    return send_from_directory(app.root_path + '/assets/backgrounds/', filename + '.edj')

@app.route('/backgrounds/preview/<path:imagename>.png')
def backgrounds_preview_assets(imagename):
    return send_from_directory(app.root_path + '/assets/backgrounds/preview/', imagename + '.png')

@app.route('/backgrounds/')
@app.route('/backgrounds/<bg_id>')
def backgrounds(bg_id=None):
    if bg_id:
        if bg_id not in BACKGROUNDS:
            abort(404)
        return render_template('background.html', background_id=bg_id, background=BACKGROUNDS[bg_id])
    else:
        return render_template('backgrounds.html', backgrounds=BACKGROUNDS)

api.add_resource(BackgroundList, '/v1/backgrounds/')
api.add_resource(Background, '/v1/backgrounds/<string:bg_id>')

if __name__ == '__main__':
    app.run(debug=False)
