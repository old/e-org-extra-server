THEMES = {
  'arc-dark': {
    'theme_id': 'arc-dark',
    'name': 'Arc Dark',
    'author': 'LeBlue',
    'description': 'Arc (dark) theme for enlightenment.',
    'source' : 'https://github.com/LeBlue/enlightenment-arc-theme.git',
    'version': 3
  },
  'arc-light': {
    'theme_id': 'arc-light',
    'name': 'Arc Light',
    'author': 'LeBlue',
    'description': 'Arc (light) theme for enlightenment.',
    'version': 3
  },
  'blingbling': {
    'theme_id': 'blingbling',
    'name': 'Bling',
    'author': 'netstar',
    'description': 'The theme used while e17 was in development.',
    'source' : 'https://git.enlightenment.org/themes/blingbling.git/',
    'version': 2
  },
  'eflemettary': {
    'theme_id': 'eflemettary',
    'name': 'Eflemettary',
    'author': 'NikaWhite, rimmed, FurryMad and LordDrew',
    'description': 'A theme written and designed with eflete',
    'source' : 'https://git.enlightenment.org/devs/nikawhite/eflemettary.git/',
    'version': 1,
  },
  'flat': {
    'theme_id': 'flat',
    'name': 'Flat',
    'author': 'raster',
    'description': 'A WORK IN PROGRESS flat theme',
    'version': 7,
  },
  'Ice': {
    'theme_id': 'Ice',
    'name': 'Ice',
    'author': 'Simotek',
    'description': 'A theme designed for Ice. Background by SiriusLee (http://wall.alphacoders.com/big.php?i=963675).',
    'source' : 'https://github.com/simotek/Enlightenment-Themes',
    'version': 1,
  }
}
BACKGROUNDS = {
  'brushed-metal': {
    'background-id': 'brushed-metal',
    'name': 'Brushed Metal',
    'author': 'netstar',
    'version': 2
  },
  'cracked-earth': {
    'background-id': 'cracked-earth',
    'name': 'Cracked Earth',
    'author': 'netstar',
    'version': 1
  },
  'ENX': {
    'background-id': 'ENX',
    'name': 'ENX',
    'author': 'netstar',
    'description': 'E logo on QNX colors',
    'version': 1
  },
  'E95': {
    'background-id': 'E95',
    'name': 'E95',
    'author': 'netstar',
    'description': 'E 1995',
    'version': 1
  },
  'highland-flood': {
    'background-id': 'highland-flood',
    'name': 'The River Dee',
    'author': 'Calvin Jones',
    'version': 1
  },
  'sky-tree': {
    'background-id': 'sky-tree',
    'name': 'Sky with Enlightenment',
    'author': 'raster',
    'version': 1
  },
  'earthviews': {
    'background-id': 'earthviews',
    'name': 'Animated earth background',
    'author': 'Brian \'morlenxus\' Miculcy',
    'version': 52
  },
  'sabana-night': {
    'background-id': 'sabana-night',
    'name': 'Sabana Night',
    'author': 'Rafael Eduardo Rumbos',
    'description': 'A night version of raster\'s sky with twinkling lights',
    'version': 2
  }
}
