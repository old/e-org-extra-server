from setuptools import setup

setup(
  name='Extra Server',
  version='1.0',
  long_description=__doc__,
  packages=['extra'],
  include_package_data=True,
  zip_safe=False,
  install_requires=[
    'Flask>=0.11.1',
    'Flask-RESTful>=0.3.5'
  ]
)


